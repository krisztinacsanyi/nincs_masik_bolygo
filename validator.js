$(document).ready(function(){
        $("#myform").validate({
            rules: {
                veznev: "required",
                kernev: "required",
                email: {
                required: true,
                email: true
                },
                szuldatum:{
                    required:true
                },
                password: {
                required: true,
                minlength: 6
                },
                szallit:{
                    required:true
                },
                feltetel:{
                    required:true
                },
                },
            messages: {
                    veznev: " Add meg a keresztneved!",
                    kernev: " Add meg a keresztneved!",
                    szuldatum: " Add meg a születési dátumodat!",
                    szallit: " Add meg a szállítási címedet!",
                    password: {
                    required: " Írd be a jelszavad!",
                    minlength: " A jelszavadnak legalább 6 karakter hosszúnak kell lennie"
                    },
                    email: " Kérlek létező email címet adj meg!",
                    feltetel: " Kérlek fogadd el az adatkezelési  feltételeinket!",
                },
                });
            });